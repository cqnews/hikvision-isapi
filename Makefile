LINK    = @echo linking $@ && arm-linux-gnueabihf-g++
GCC     = @echo compiling $@ && arm-linux-gnueabihf-g++ 
GC      = @echo compiling $@ && arm-linux-gnueabihf-gcc
AR      = @echo generating static library $@ && ar crv 
FLAGS   = -g -W -Wall -fPIC -std=c++11 -Wno-unused-parameter
GCCFLAGS = 
DEFINES = 
HEADER  = -I./ -I/usr/arm32/include 
LIBS    = -L./ -L/usr/arm32/lib 
LINKFLAGS =

LIBS    += -pthread -lev -lcurl -lcrypto -lssl -lm 

SOURCE_FILES :=\
		main.cpp\
		app/camera/hikvision.cpp\
		app/functions/functions.cpp\
		app/functions/md5.cpp\
		app/functions/tinyxml2.cpp\
		app/web/httpserver.cpp\
		app/web/mongoose.c\


PROJECTNAME = HikvisionISAPI.out

TARGET = main

all: $(SOURCE_FILES)
	$(LINK) $(FLAGS) $(LINKFLAGS)  -o ${PROJECTNAME} $^ $(LIBS) ${HEADER} -DDEBUG
clean:
	rm -rf *.o ${PROJECTNAME} *.out bin obj
del:
	rm -rf *
