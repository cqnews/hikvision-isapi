﻿#ifndef __HIKCAMERA_H__
#define __HIKCAMERA_H__

#include <string.h>
#include <curl/curl.h>
#include <iostream>
#include "../functions/md5.h"



#define HTTP_MAX_REALM_LEN		32
#define HTTP_MAX_NONCE_LEN		64
#define HTTP_DEGIST_SRC_LEN     528
#define HTTP_HA_LEN             128

typedef struct CAMERA_INFO
{
    char ip[20]; //ip地址
    int port = 80; //端口号
    char username[20]; //用户名
    char password[32]; //密码
    bool isSuccess;
}CameraInfo;

typedef enum ISAPI_PROTOCOL_COMMAND
{
    ISAPI_GET = 0,
    ISAPI_PUT = 1,
    ISAPI_POST = 2,
    ISAPI_DELETE = 3,
}ISAPI_PROTOCOL_COMMAND, IsapiProtocolCommand;


class HikCamera
{
private:
    HikCamera();
    ~HikCamera();
public:
    CameraInfo* camera;

public:
    int init(std::string ip, int port, std::string username, std::string passowrd); //初始化摄像机参数
    bool isInit();
    int putCapture(int channel, const char* filename); //摄像机抓图
    std::string getRtspUrl(); //获取摄像机参数信息
    //摄像机旋转控制
    //摄像机水平旋转,正数为顺时针旋转,负数为逆时针,数字大小代表旋转快慢
    //摄像机上下旋转,正数为顺时针旋转,负数为逆时针,数字大小代表旋转快慢
    //摄像机焦距调整,正数放大,负数缩小
    int putControl(int id, int pan, int tilt, int zoom);
    int reboot(); //摄像机设备重启
    int factoryDefault(); //恢复出厂设置
    int devInfo();//获取设备信息
    int devStatus(); //获取设备状态
private:
    int initCurl(CURL** curl, struct curl_slist* headers); //初始化Curl
    int clearCurl(CURL** curl, struct curl_slist* headers); //清空Curl
    int generateAuthorization(IsapiProtocolCommand protocolCommand, const char* protocolUrl, char* authorizationBuf);
    static size_t readStringBuffFunction(char* contents, size_t size, size_t nmemb, void* userStr);
    size_t readStringKey(const char* sourceChr, const char* key, char* returnChr);
private:
    static HikCamera* m_instance_ptr; //单例变量
public:
    static pthread_mutex_t mutex;
    static HikCamera* getInstance(); //单例实例化函数
};

#endif
