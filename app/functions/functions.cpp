#include "functions.h"

/******************************************************************************
*  Name        :   Function::delay
*  Author      :   cqnews
*  Version     :   V1.0.0
*  Data        :   2020.12.17
*  Describe    :   获取unix时间戳
*  time        :   延迟时间 time*1000000为秒数
******************************************************************************/
void Functions::delay(const int time, const int expire)
{
    sleep(time);
    UNUSED(expire);
    //clock_t now = clock();
    //clock_t delay_time = 1000000 * time;
    //clock_t expire_time = 1000000 * expire;


    //while (1)
    //{

    //    clock_t t = clock() - now;

    //    if (t >= delay_time || t > expire_time || t <= 0) //正常时间判断
    //    {
    //        break;
    //    }
    //};
}

/******************************************************************************
*  Name        :   Function::random2
*  Author      :   cqnews
*  Version     :   V1.0.0
*  Data        :   2020.12.17
*  Describe    :   获取unix时间戳
*  time        :   随机数
******************************************************************************/
int Functions::random2(int start, int end, time_t t)
{
    srand((unsigned int)t);
    return (rand() % (end - start + 1)) + start;
}



/******************************************************************************
*  Name        :   Timer::getUnixTimestamp
*  Author      :   cqnews
*  Version     :   V1.0.0
*  Data        :   2020.12.17
*  Describe    :   获取unix时间戳
******************************************************************************/
time_t Timer::get_unix_timestamp()
{
    try
    {
        const time_t t = time(NULL);

#ifdef DEBUG
        std::cout << "sizeof(time_t) is: " << sizeof(time_t) << std::endl;
#endif

        return t;
    }
    catch (std::exception& e)
    {
    }


    return -1;
}



/******************************************************************************
 *  Name        :   Timer::format_datetime
 *  Author      :   cqnews
 *  Version     :   V1.0.0
 *  Data        :   2021.04.13
 *  Describe    :   数字转时间戳
 ******************************************************************************/
time_t Timer::format_datetime(int uiYear, int uiMonth, int uiDay, int uiHour, int uiMinute, int uiSecond)
{
    printf("format_datetime,uiYear:%d\n", uiYear);

    tm t;                                    // 定义tm结构体。
    memset(&t, 0, sizeof(tm));

    t.tm_year = uiYear - 1900;                 // 年，由于tm结构体存储的是从1900年开始的时间，所以tm_year为int临时变量减去1900。
    t.tm_mon = uiMonth - 1;                    // 月，由于tm结构体的月份存储范围为0-11，所以tm_mon为int临时变量减去1。
    t.tm_mday = uiDay;                         // 日。
    t.tm_hour = uiHour;                        // 时。
    t.tm_min = uiMinute;                       // 分。
    t.tm_sec = uiSecond;                       // 秒。
    t.tm_isdst = 0;                          // 非夏令时。
    time_t t_ = mktime(&t);                  // 将tm结构体转换成time_t格式。

    return t_;                                 // 返回值。
}
