#ifndef __FUNCTION_H__
#define __FUNCTION_H__


#include <exception>
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <string.h>

#define UNUSED(x) (void)x

#ifndef NOMINMAX

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#endif  /* NOMINMAX */

#ifndef DEBUG
#define DEBUG
#endif // !DEBUG


class Functions
{
public:

    /******************************************************************************
    *  Name        :   Function::delay
    *  Author      :   cqnews
    *  Version     :   V1.0.0
    *  Data        :   2020.12.17
    *  Describe    :   获取unix时间戳
    *  time        :   延迟时间 time*1000000为秒数
    ******************************************************************************/
    static void delay(const int time, const int expire = 360);//延迟函数 获取unix时间戳

    /******************************************************************************
    *  Name        :   Function::random2
    *  Author      :   cqnews
    *  Version     :   V1.0.0
    *  Data        :   2020.12.17
    *  Describe    :   获取unix时间戳
    *  time        :   随机数
    ******************************************************************************/
    static int random2(int start, int end, time_t t); //生成范围内的随机数
};

class Timer
{
public:

    /******************************************************************************
     *  Name        :   Timer::getUnixTimestamp
     *  Author      :   cqnews
     *  Version     :   V1.0.0
     *  Data        :   2020.12.17
     *  Describe    :   获取unix时间戳
     ******************************************************************************/
    static time_t get_unix_timestamp();

    /******************************************************************************
    *  Name        :   Timer::format_datetime
    *  Author      :   cqnews
    *  Version     :   V1.0.0
    *  Data        :   2021.04.13
    *  Describe    :   数字转时间戳
    ******************************************************************************/
    static time_t format_datetime(int uiYear, int uiMonth, int uiDay, int uiHour, int uiMinute, int uiSecond);


};

#endif // !__FUNCTION_H__
