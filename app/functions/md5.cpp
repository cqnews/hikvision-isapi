#include "md5.h"
#include <openssl/md5.h>



int Md5::md5(char* dataChr, size_t len, char* md5Chr)
{
    MD5_CTX ctx;
    if (dataChr == NULL || len <= 0 || md5Chr == NULL)
    {
        printf("Input param invalid!");
        return -1;
    }

    memset(&ctx, 0, sizeof(ctx));
    MD5_Init(&ctx);
    MD5_Update(&ctx, dataChr, len);

    unsigned char md5[16] = { 0 };
    MD5_Final(md5, &ctx);

    for (int i = 0; i < 16; i++)
    {
        snprintf(md5Chr + i * 2, 2 + 1, "%02x", md5[i]);
    }

    md5Chr[16 * 2] = '\0';

    return 0;
}