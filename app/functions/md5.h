/**
 * @file md5.h
 * @The header file of md5.
 * @author Jiewei Wei
 * @mail weijieweijerry@163.com
 * @github https://github.com/JieweiWei
 * @data Oct 19 2014
 *
 */

#ifndef __MD5_H__
#define __MD5_H__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

class Md5
{
public:
   static int md5(char* dataChr, size_t len, char* md5Chr);
private:

};


#endif // __MD5_H__
