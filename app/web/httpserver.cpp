#include "httpserver.h"
#include "../functions/tinyxml2.h"
#include "../camera/hikvision.h"

using namespace tinyxml2;

/// <summary>
/// 解析默认首页
/// </summary>
/// <param name="nc"></param>
/// <param name="ev"></param>
/// <param name="p"></param>
__attribute__((unused)) static void handleHomePage(struct mg_connection* nc, int ev, void* p)
{
    UNUSED(ev);
    UNUSED(p);
    std::string bodyStr = "Welcome to 510Link.com";

    mg_printf(nc, "HTTP/1.1 200 OK\r\n"
        "Content-Type: application/json\r\n"
        "Content-Length: %d\r\n"
        "\r\n"
        "%s",
        (int)bodyStr.length(), bodyStr.c_str());

    nc->flags |= MG_F_SEND_AND_CLOSE;
}

/// <summary>
/// 解析alarm报警信息
/// </summary>
/// <param name="nc"></param>
/// <param name="ev"></param>
/// <param name="p"></param>
__attribute__((unused)) static void handleAlarmPage(struct mg_connection* nc, int ev, void* p)
{
    UNUSED(ev);
    UNUSED(p);

    if (ev == MG_EV_HTTP_REQUEST)
    {
        char addr[32];
        struct http_message* hm = (struct http_message*)p;
        mg_sock_addr_to_str(&nc->sa, addr, sizeof(addr),
            MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);

        std::cout << "addr:" << addr << std::endl;

        char body[1024] = { 0 };
        int len = min(1024, hm->body.len);
        memcpy(body, hm->body.p, len);
        if (body == NULL) { return; }

        std::cout << "hm->body.p:" << body << std::endl;

        XMLDocument xmlDocument;
        XMLError xmlError = xmlDocument.Parse(body, len);
        std::cout << "xmlError:" << xmlError << std::endl;

        if (xmlError != XML_SUCCESS)
        {
            std::string errorStr = "Welcome to 510Link.com,xml error";

            mg_printf(nc, "HTTP/1.1 500 OK\r\n"
                "Content-Type: application/json\r\n"
                "Content-Length: %d\r\n"
                "\r\n"
                "%s",
                (int)errorStr.length(), errorStr.c_str());

            return;
        }

        XMLElement* rootXmlElement = xmlDocument.FirstChildElement("EventNotificationAlert");

        if (rootXmlElement == NULL)
        {

            std::string errorStr = "Welcome to 510Link.com,xml root error";

            mg_printf(nc, "HTTP/1.1 500 OK\r\n"
                "Content-Type: application/json\r\n"
                "Content-Length: %d\r\n"
                "\r\n"
                "%s",
                (int)errorStr.length(), errorStr.c_str());

            return;
        }

        XMLElement* childXmlElement = rootXmlElement->FirstChildElement("ipAddress");
        if (childXmlElement)
        {
            std::cout << "ipAddress:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("portNo");
        if (childXmlElement)
        {
            std::cout << "portNo:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("protocol");
        if (childXmlElement)
        {
            std::cout << "protocol:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("macAddress");
        if (childXmlElement)
        {
            std::cout << "macAddress:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("channelID");
        if (childXmlElement)
        {
            std::cout << "channelID:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("dateTime");
        if (childXmlElement)
        {
            std::cout << "dateTime:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("activePostCount");
        if (childXmlElement)
        {
            std::cout << "activePostCount:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("eventType");
        if (childXmlElement)
        {
            std::cout << "eventType:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("eventState");
        if (childXmlElement)
        {
            std::cout << "eventState:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("eventDescription");
        if (childXmlElement)
        {
            std::cout << "eventDescription:" << childXmlElement->GetText() << std::endl;
        }

        childXmlElement = rootXmlElement->FirstChildElement("channelName");
        if (childXmlElement)
        {
            std::cout << "channelName:" << childXmlElement->GetText() << std::endl;
        }

        std::string bodyStr = "Welcome to 510Link.com,ok";

        mg_printf(nc, "HTTP/1.1 200 OK\r\n"
            "Content-Type: application/json\r\n"
            "Content-Length: %d\r\n"
            "\r\n"
            "%s",
            (int)bodyStr.length(), bodyStr.c_str());
    }

    nc->flags |= MG_F_SEND_AND_CLOSE;

}


__attribute__((unused)) static void handleUpload(struct mg_connection* nc, int ev, void* p)
{
    UNUSED(ev);
    UNUSED(p);
    std::string bodyStr = "I'm upload";

    mg_printf(nc, "HTTP/1.1 200 OK\r\n"
        "Content-Type: application/json\r\n"
        "Content-Length: %d\r\n"
        "\r\n"
        "%s",
        (int)bodyStr.length(), bodyStr.c_str());

    nc->flags |= MG_F_SEND_AND_CLOSE;
}

__attribute__((unused)) static void handeleOtherPage(struct mg_connection* nc, int ev, void* p)
{

    //struct mg_serve_http_opts s_http_server_opts;


    if (ev == MG_EV_HTTP_REQUEST)
    {

        char addr[32];
        struct http_message* hm = (struct http_message*)p;
        mg_sock_addr_to_str(&nc->sa, addr, sizeof(addr),
            MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);

        printf("HTTP request from %s: %.*s %.*s\n", addr, (int)hm->method.len,
            hm->method.p, (int)hm->uri.len, hm->uri.p);

        char uri[30] = { 0 };
        memcpy(uri, hm->uri.p, hm->uri.len);

        std::string method = std::string(hm->method.p, hm->method.len);

        std::string str_out = "HTTP/1.1 200 OK\r\n"
            "Access-Control-Allow-Origin: *\r\n"
            "Access-Control-Allow-Methods: *\r\n"
            "Content-Type: application/json;charset=UTF-8\r\n"
            "Server: 510link httpd 1.0\r\n"
            "Connection: close\r\n"
            "\r\n";

        if (strcmp("/", uri) == 0 || strcmp("/index", uri) == 0)
        {
            str_out = str_out + std::string("{\"content\":\"Welcome 510link.com\"}");
        }
        else if (strcmp("/api/camera/record", uri) == 0)
        {

            str_out = str_out + std::string("{\"content\":\"Welcome API 510link.com\"}");
        }
        else
        {
            str_out = str_out + std::string("{\"content\":\"error addr\"}");
        }

        mg_printf(nc, str_out.c_str());
        nc->flags |= MG_F_SEND_AND_CLOSE;

        //mg_serve_http(nc, (struct http_message*)p, s_http_server_opts);
    }
}


/******************************************************************************
*  Name        :   PTTcpServer::net_server_pthread
*  Author      :   cqnews
*  Version     :   V1.0.0
*  Date        :   2021.04.13
*  Describe    :   初始化线程
******************************************************************************/
void* HttpServer::http_server_pthread(void* args)
{
    UNUSED(args);

    struct mg_mgr mgr;
    struct mg_connection* nc;

    mg_mgr_init(&mgr, NULL);

    printf("Starting web server on port %s\n", s_http_port);

    nc = mg_bind(&mgr, s_http_port, handeleOtherPage);
    if (nc == NULL)
    {
        printf("Failed to create listener\n");
        return 0;
    }

    mg_set_protocol_http_websocket(nc);

    mg_register_http_endpoint(nc, "/", handleHomePage);
    mg_register_http_endpoint(nc, "/UploadVideo", handleUpload);
    mg_register_http_endpoint(nc, "/alarm", handleAlarmPage);

    for (;;)
    {
        mg_mgr_poll(&mgr, 1000);
    }
    mg_mgr_free(&mgr);

    return 0;
}