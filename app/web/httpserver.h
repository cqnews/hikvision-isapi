#ifndef __HTTPSERVER_H__
#define __HTTPSERVER_H__

#include <cstdio>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "mongoose.h"
#include "../functions/functions.h"

static const char s_http_port[] = "80";

class HttpServer
{
public:

    /******************************************************************************
    *  Name        :   HttpServer::http_server_pthread
    *  Author      :   cqnews
    *  Version     :   V1.0.0
    *  Date        :   2021.04.13
    *  Describe    :   ��ʼ���߳�
    ******************************************************************************/
    static void* http_server_pthread(void* args);

};

#endif //__HTTPSERVER_H__